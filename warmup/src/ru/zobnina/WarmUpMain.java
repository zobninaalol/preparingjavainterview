package ru.zobnina;

public class WarmUpMain {
    public static void main(String[] args) {
        Node n14 = new Node();
        n14.setData(10);
        Node n13 = new Node();
        n13.setData(7);
        n13.setNext(n14);
        Node n12 = new Node();
        n12.setData(5);
        n12.setNext(n13);
        Node n11 = new Node();
        n11.setData(2);
        n11.setNext(n12);
        Node head1 = new Node();
        head1.setData(7);
        head1.setNext(n11);

        Node n23 = new Node();
        n23.setData(8);
        Node n22 = new Node();
        n22.setData(4);
        n22.setNext(n23);
        Node n21 = new Node();
        n21.setData(1);
        n21.setNext(n22);
        Node head2 = new Node();
        head2.setData(1);
        head2.setNext(n21);
        Node head = merge(head1, head2);
    }

    static Node merge(Node head1, Node head2) {
        if (head1 == null) return head2;
        if (head2 == null) return head1;
        Node head = new Node();
        Node current1 = head1;
        Node current2 = head2;
        if (head1.getData() < head2.getData()) {
            head.setData(head1.getData());
            current1 = head1.getNext();
        } else {
            head.setData(head2.getData());
            current2 = head2.getNext();
        }
        Node current = head;
        while (current != null) {
            Node newNode;
            if (current1 == null) {
                current.setNext(current2);
                break;
            } else if (current2 == null) {
                current.setNext(current1);
                break;
            } else {
                newNode = new Node();
                if (current1.getData() < current2.getData()) {
                    newNode.setData(current1.getData());
                    current1 = current1.getNext();
                } else {
                    newNode.setData(current2.getData());
                    current2 = current2.getNext();
                }
            }
            current.setNext(newNode);
            current = current.getNext();
        }
        return head;
    }
}

